const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getCloneId, getIdentity, getCloneData, getPlaceData } = require('./setup');

describe('Collection places.clones', () => {

  it('should allow clone reference creation only by the user ID set as relation', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));

    const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice'));

    const alice = getAuthedFirestore(getIdentity());
    const bob = getAuthedFirestore(getIdentity('bob'));
    const charlie = getAuthedFirestore(getIdentity('charlie'));

    await firebase.assertFails(
      charlie.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
    );
    await firebase.assertFails(
      bob.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
    );
    await firebase.assertSucceeds(
      alice.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
    );
  });

  it('should allow only receiverId and relation fields', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));

    const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.doc(cloneDoc.path).set({})
    );

    const {relation, receiverId} = getCloneData('bob', 'alice');
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        relation
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation,
        other: 'test'
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation,
        placeId: 'paris'
      })
    );
    await firebase.assertSucceeds(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation
      })
    );
  });

  [
    {valid: false, receiverId: ''},
    {valid: false, receiverId: 'ali.ce'},
    {valid: false, receiverId: 'ali/c/e'},
    {valid: false, receiverId: 'ali[ce'},
    {valid: false, receiverId: 'ali]ce'},
    {valid: false, receiverId: 'ali\\ce'},
    {valid: false, receiverId: 'ali`ce'},
    {valid: false, receiverId: 'alice'.padEnd(129, 'e')},
    {valid: true, receiverId: 'alice'},
    {valid: true, receiverId: 'alice123-_'},
    {valid: true, receiverId: 'alice'.padEnd(128, 'e')},
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the user segment path ${testCase.receiverId.slice(0, 50)}`, async () => {
      const admin = getAdminFirestore();
      const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId(testCase.receiverId, 'alice'));

      if (testCase.receiverId.length > 0) {
        await admin.collection('users').doc(testCase.receiverId)
            .collection('followed').doc('alice')
            .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));
      }
      const db = getAuthedFirestore(getIdentity());
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        db.doc(cloneDoc.path).set(getCloneData(testCase.receiverId, 'alice'))
      );
    });
  });

  [
    {valid: false, id: 'alice_bob'},
    {valid: false, id: 'bob_alice_2'},
    {valid: false, id: '_bob_alice'},
    {valid: false, id: 'bob_alic'},
    {valid: true, id: 'bob_alice'},
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the clone ID ${testCase.id}`, async () => {
      const admin = getAdminFirestore();
      const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(testCase.id);
      await admin.collection('users').doc('bob')
          .collection('followed').doc('alice')
          .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));

      const db = getAuthedFirestore(getIdentity());
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        db.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
      );
    });
  });

  it('should allow clone reference creation only if the object is also created', async () => {
    const admin = getAdminFirestore();
    const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
    );

    await admin.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));

    await firebase.assertSucceeds(
      db.doc(cloneDoc.path).set(getCloneData('bob', 'alice'))
    );
  });

  it('should allow clone deletion only by the receiver ID or the user ID set as relation', async () => {
    const admin = getAdminFirestore();

    const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId('alice', 'bob'));
    await cloneDoc.set(getCloneData('alice', 'bob'));

    const alice = getAuthedFirestore(getIdentity());
    const bob = getAuthedFirestore(getIdentity('bob'));
    const charlie = getAuthedFirestore(getIdentity('charlie'));

    await firebase.assertFails(
      charlie.doc(cloneDoc.path).delete()
    );
    await firebase.assertSucceeds(
      alice.doc(cloneDoc.path).delete()
    );
    await firebase.assertFails(
      bob.doc(cloneDoc.path).delete()
    );
    await cloneDoc.set(getCloneData('alice', 'bob'));
    await firebase.assertSucceeds(
      bob.doc(cloneDoc.path).delete()
    );
  });

  it('should allow clone reference deletion only if the object is also deleted', async () => {
    const admin = getAdminFirestore();

    const followedPlaceDoc = admin.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris');
    const cloneDoc = admin.collection('places').doc('paris').collection('clones').doc(getCloneId('alice', 'bob'));
    await followedPlaceDoc.set(getPlaceData('bob', '2021-01-01T00:00:00Z', true));
    await cloneDoc.set(getCloneData('alice', 'bob'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.doc(cloneDoc.path).delete()
    );

    const batch = db.batch();
    batch.delete(db.doc(followedPlaceDoc.path));
    batch.delete(db.doc(cloneDoc.path));
    await firebase.assertSucceeds(
      batch.commit()
    );
  });
});
