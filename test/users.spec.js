const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getIdentity, getUserData } = require('./setup');

describe('Collection users', () => {
  it('require users to log in to read their profile', async () => {
    const db = getAuthedFirestore(null);
    const doc = db.collection('users').doc('alice');
    await firebase.assertFails(doc.get());
  });

  it('require users to have a verified email to read their profile', async () => {
    const db = getAuthedFirestore({ uid: 'alice', email_verified: false, firebase: {sign_in_provider: 'google.com'}});
    const doc = db.collection('users').doc('alice');
    await firebase.assertFails(doc.get());
  });

  it('require users to use an allowed auth provider to read their profile', async () => {
    const customProvider = getAuthedFirestore({ uid: 'alice', email_verified: true, firebase: {sign_in_provider: 'custom'}});
    const passwordProvider = getAuthedFirestore({ uid: 'alice', email_verified: true, firebase: {sign_in_provider: 'password'}});
    const googleProvider = getAuthedFirestore({ uid: 'alice', email_verified: true, firebase: {sign_in_provider: 'google.com'}});
    const appleProvider = getAuthedFirestore({ uid: 'alice', email_verified: true, firebase: {sign_in_provider: 'apple.com'}});
    await firebase.assertFails(customProvider.collection('users').doc('alice').get());
    await firebase.assertSucceeds(passwordProvider.collection('users').doc('alice').get());
    await firebase.assertSucceeds(googleProvider.collection('users').doc('alice').get());
    await firebase.assertSucceeds(appleProvider.collection('users').doc('alice').get());
  });

  it('should only let users read their own profile', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('bob');
    await firebase.assertFails(doc.get());
  });

  it('should only let users write their own profile', async () => {
    const db = getAuthedFirestore(getIdentity());
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').set(getUserData())
    );
    await firebase.assertFails(
      db.collection('users').doc('bob').set(getUserData())
    );
  });

  it('should allow only the email, displayName, photoURL and deletionDate fields in user profiles', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice');
    const {email, displayName, photoURL, deletionDate} = getUserData();

    await firebase.assertFails(
      doc.set({})
    );
    await firebase.assertFails(
      doc.set({email})
    );
    await firebase.assertFails(
      doc.set({deletionDate})
    );
    await firebase.assertSucceeds(
      doc.set({email, deletionDate})
    );
    await firebase.assertSucceeds(
      doc.set({email, displayName, deletionDate})
    );
    await firebase.assertSucceeds(
      doc.set({email, photoURL, deletionDate})
    );
    await firebase.assertSucceeds(
      doc.set({email, displayName, photoURL, deletionDate})
    );
    await firebase.assertFails(
      doc.set({email, displayName, photoURL, deletionDate, other: 'field'})
    );
  });

  it('should limit the fields size of user profiles', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice');

    await firebase.assertFails(
      doc.set(getUserData({displayName: ''}))
    );
    await firebase.assertFails(
      doc.set(getUserData({photoURL: ''}))
    );
    await firebase.assertFails(
      doc.set(getUserData({displayName: ''.padEnd(251, '.')}))
    );
    await firebase.assertFails(
      doc.set(getUserData({photoURL: 'https://example.com/'.padEnd(251, '.')}))
    );
    await firebase.assertSucceeds(
      doc.set(getUserData({
        displayName: ''.padEnd(250, '.'),
        photoURL: 'https://example.com/'.padEnd(250, '.'),
      }))
    );
  });

  it('should verify that the email is aligned with auth info', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice');

    await firebase.assertFails(
      doc.set(getUserData({
        email: 'bob@example.com',
      }))
    );
  });

  [
    { valid: true, email: 'my.name+alias@example.com' },
    { valid: true, email: 'Ab0._%+-@Cd1-..Ef' },
    { valid: true, email: '1@2.ab' },
    { valid: true, email: 'alice@example'.padEnd(246, '.') + '.com' },
    { valid: false, email: '' },
    { valid: false, email: 'invalid' },
    { valid: false, email: '@example.com' },
    { valid: false, email: 'alice@.com' },
    { valid: false, email: 'alice@example.c' },
    { valid: false, email: 'alice@192.168.0.1' },
    { valid: false, email: 'alice@example'.padEnd(247, '.') + '.com' },
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the format of the email ${testCase.email.slice(0, 50)}`, async () => {
      const db = getAuthedFirestore({...getIdentity(), email: testCase.email});
      const doc = db.collection('users').doc('alice');
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(doc.set(getUserData({
        email: testCase.email,
      })));
    });
  });

  it('should verify that photoURL is a valid URL', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice');

    await firebase.assertFails(doc.set(getUserData({
      photoURL: 'this is not an URL'
    })));
    await firebase.assertFails(doc.set(getUserData({
      photoURL: 'ftp://invalid-scheme.com'
    })));
    await firebase.assertFails(doc.set(getUserData({
      photoURL: 'http://'
    })));
    await firebase.assertFails(doc.set(getUserData({
      photoURL: 'http://!.com'
    })));
    await firebase.assertFails(doc.set(getUserData({
      photoURL: 'xhttp://example.com'
    })));

    await firebase.assertSucceeds(doc.set(getUserData({
      photoURL: 'http://example.com'
    })));
    await firebase.assertSucceeds(doc.set(getUserData({
      photoURL: 'https://example.com/path/to/file.jpg'
    })));
    await firebase.assertSucceeds(doc.set(getUserData({
      photoURL: 'http://user:password@example.com:80/path_to-/file.txt?param=+%~&#'
    })));
  });

  it('should ensure that the deletionDate is valid', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice');

    await firebase.assertFails(doc.set(getUserData({
      deletionDate: 'not a date'
    })));
    await firebase.assertFails(doc.set(getUserData({
      deletionDate: undefined
    })));
    await firebase.assertFails(doc.set(getUserData({
      deletionDate: '2021-01-01T00:00:00Z'
    })));
    await firebase.assertSucceeds(doc.set(getUserData({
      deletionDate: null
    })));

    await firebase.assertFails(doc.update({
      deletionDate: 'not a date'
    }));
    await firebase.assertFails(doc.update({
      deletionDate: firebase.firestore.FieldValue.delete()
    }));
    await firebase.assertFails(doc.update({
      deletionDate: '2021-01-01T00:00:00Z'
    }));
    await firebase.assertSucceeds(doc.update({
      deletionDate: firebase.firestore.FieldValue.serverTimestamp()
    }));
  });

  it('should prevent profile deletion', async () => {
    const db = getAuthedFirestore(getIdentity());
    await firebase.assertFails(db.collection('users').doc('alice').delete());
    await firebase.assertFails(db.collection('users').doc('bob').delete());
  });
});
