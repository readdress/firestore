const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getIdentity, getPrivateData } = require('./setup');

describe('Collection users.private', () => {
  it('should only let users read their own private account', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('private').doc('account').get()
    );
    await firebase.assertFails(
      db.collection('users').doc('bob').collection('private').doc('account').get()
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('private').doc('alice').get()
    );
  });

  it('should only let users set their own private info', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('private').doc('account').set(getPrivateData())
    );
    await firebase.assertFails(
      db.collection('users').doc('bob').collection('private').doc('account').set(getPrivateData())
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('private').doc('alice').set(getPrivateData())
    );
  });

  it('should allow and enforce only the authorized fields in user private account', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');

    await firebase.assertFails(
      doc.set(getPrivateData({
        other: 'other'
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        creationDate: undefined
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: undefined
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        language: undefined
      }))
    );
    await firebase.assertSucceeds(
      doc.set(getPrivateData())
    );
  });


  it('should set the creation date at creation and never change it after', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');

    await firebase.assertFails(
      doc.set(getPrivateData({
        creationDate: 'invalid date'
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        creationDate: '2021-01-01T01:00:00Z'
      }))
    );
    await firebase.assertSucceeds(
      doc.set(getPrivateData())
    );
    await firebase.assertFails(
      doc.update({
        creationDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      doc.update({
        creationDate: (await doc.get()).get('creationDate'),
      })
    );
  });

  it('should verify that the devices field is a map of an allowed size', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');

    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: 'invalid map'
      }))
    );
    await firebase.assertSucceeds(
      doc.set(getPrivateData({
        devices: {}
      }))
    );

    const devices = {};
    for (let i = 0; i < 10; i++) {
      devices[`messaging_token_${i}`] = firebase.firestore.FieldValue.serverTimestamp();
    }
    await firebase.assertSucceeds(
      doc.update({
        devices
      })
    );

    devices['messaging_token_10'] = firebase.firestore.FieldValue.serverTimestamp();
    await firebase.assertFails(
      doc.update({
        devices
      })
    );
  });

  it('should ensure that the device\'s token is a string with an allowed size', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');

    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          1: firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          [''.padEnd(9, 'a')]: firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          [''.padEnd(501, 'a')]: firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          'messaging_token.2': firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertSucceeds(
      doc.set(getPrivateData({
        devices: {
          [''.padEnd(10, 'a')]: firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertSucceeds(
      doc.update({
        devices: {
          [''.padEnd(500, 'a')]: firebase.firestore.FieldValue.serverTimestamp()
        }
      })
    );
  });

  it('should ensure that the device\'s value is now or unchanged', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');


    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          messaging_token: 'invalid date'
        }
      }))
    );
    await firebase.assertFails(
      doc.set(getPrivateData({
        devices: {
          messaging_token: '2021-01-01T01:00:00Z'
        }
      }))
    );
    await firebase.assertSucceeds(
      doc.set(getPrivateData({
        devices: {
          messaging_token: firebase.firestore.FieldValue.serverTimestamp()
        }
      }))
    );
    await firebase.assertSucceeds(
      doc.update({
        devices: {
          messaging_token: (await doc.get()).get('devices.messaging_token'),
          messaging_token_2: firebase.firestore.FieldValue.serverTimestamp(),
        }
      })
    );
    await firebase.assertSucceeds(
      doc.update({
        devices: {
          messaging_token: firebase.firestore.FieldValue.serverTimestamp(),
          messaging_token_2: (await doc.get()).get('devices.messaging_token_2'),
        }
      })
    );
    await firebase.assertFails(
      doc.update({
        devices: {
          messaging_token_2: '2021-01-01T01:00:00Z'
        }
      })
    );
  });


  it('should verify that the language field is an allowed language code', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice').collection('private').doc('account');

    await firebase.assertSucceeds(
      doc.set(getPrivateData())
    );

    await firebase.assertFails(
      doc.update({
        language: ''
      })
    );
    await firebase.assertFails(
      doc.update({
        language: 'invalid'
      })
    );
    await firebase.assertFails(
      doc.update({
        language: 'en_USS'
      })
    );
    await firebase.assertFails(
      doc.update({
        language: 'en-US'
      })
    );
    await firebase.assertSucceeds(
      doc.update({
        language: 'en_US'
      })
    );
    await firebase.assertSucceeds(
      doc.update({
        language: 'fr'
      })
    );
  });

  it('should prevent private info deletion', async () => {
    const db = getAuthedFirestore(getIdentity());
    await firebase.assertFails(db.collection('users').doc('alice').collection('private').doc('account').delete());
    await firebase.assertFails(db.collection('users').doc('bob').collection('private').doc('bob').delete());
  });
});
