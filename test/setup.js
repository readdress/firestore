const firebase = require('@firebase/rules-unit-testing');
const fs = require('fs');
const http = require('http');

/**
 * The emulator will accept any project ID for testing.
 */
const PROJECT_ID = 'readdress-unit-test';

/**
 * The FIRESTORE_EMULATOR_HOST environment variable is set automatically
 * by 'firebase emulators:exec'
 */
const COVERAGE_URL = `http://${process.env.FIRESTORE_EMULATOR_HOST}/emulator/v1/projects/${PROJECT_ID}:ruleCoverage.html`;

beforeEach(async () => {
  // Clear the database between tests
  await firebase.clearFirestoreData({ projectId: PROJECT_ID });
});

before(async () => {
  // Load the rules file before the tests begin
  const rules = fs.readFileSync('firestore.rules', 'utf8');
  await firebase.loadFirestoreRules({ projectId: PROJECT_ID, rules });
});

after(async () => {
  // Delete all the FirebaseApp instances created during testing
  // Note: this does not affect or clear any data
  await Promise.all(firebase.apps().map((app) => app.delete()));

  // Write the coverage report to a file
  const coverageFile = 'firestore-coverage.html';
  const fstream = fs.createWriteStream(coverageFile);
  await new Promise((resolve, reject) => {
    http.get(COVERAGE_URL, (res) => {
      res.pipe(fstream, { end: true });

      res.on('end', resolve);
      res.on('error', reject);
    });
  });

  console.log(`View firestore rule coverage information at ${coverageFile}\n`);
});

/**
 * Creates a new client FirebaseApp with authentication and returns the Firestore instance.
 */
exports.getAuthedFirestore = function (auth) {
  return firebase
    .initializeTestApp({ projectId: PROJECT_ID, auth })
    .firestore();
};

/**
 * Creates a new admin FirebaseApp which bypass the firestore rules.
 */
exports.getAdminFirestore = function () {
  return firebase
    .initializeAdminApp({ projectId: PROJECT_ID })
    .firestore();
};

/**
 * Export valid common stubs
 */
exports.mergeObjects = function (obj1, obj2) {
  if (!obj2) {
    return obj1;
  }
  const newObj = {
    ...obj1,
    ...obj2
  }
  Object.keys(newObj).forEach(key => newObj[key] === undefined && delete newObj[key]);
  return newObj;
};

exports.getIdentity = function (uid) {
  uid = uid || 'alice';
  return {
    uid,
    email: `${uid}@example.com`,
    email_verified: true,
    firebase: {
      sign_in_provider: 'password'
    }
  };
};

exports.getUserData = function (overwrite, uid) {
  uid = uid || 'alice';
  return exports.mergeObjects({
    email: `${uid}@example.com`,
    displayName: (uid.slice(0, 1).toUpperCase() + uid.slice(1)),
    photoURL: 'https://example.com/photo.jpg',
    deletionDate: null
  }, overwrite);
};

exports.getPrivateData = function (overwrite) {
  return exports.mergeObjects({
    creationDate: firebase.firestore.FieldValue.serverTimestamp(),
    devices: {
      messaging_token: firebase.firestore.FieldValue.serverTimestamp(),
    },
    language: 'en'
  }, overwrite);
};

exports.getFollowerData = function (overwrite, uid) {
  uid = uid || 'bob';
  return exports.mergeObjects({
    ...exports.getUserData(uid),
    status: 'pending',
    updateDate: firebase.firestore.FieldValue.serverTimestamp(),
  }, overwrite);
}

exports.adminFollowerData = function (overwrite, uid) {
  return exports.mergeObjects({
    ...exports.getFollowerData(uid),
    updateDate: '2021-01-01T00:00:00Z',
  }, overwrite);
}

exports.getFollowedData = function (overwrite, uid) {
  uid = uid || 'bob';
  return exports.mergeObjects({
    ...exports.getUserData(uid),
    status: 'allowed',
  }, overwrite);
}

exports.getCloneId = function (receiverId, relation) {
  return (receiverId !== undefined ? receiverId : '')
    + '_'
    + (relation !== undefined ? relation : '');
};

exports.getCloneData = function (receiverId, relation) {
  receiverId = receiverId || 'alice';
  relation = relation || 'followers';
  return {
    receiverId,
    relation,
  };
};

exports.getPlaceData = function (ownerId, creationDate, skipGeopoint) {
  ownerId = (ownerId === undefined) ? 'alice' : ownerId;
  creationDate = (creationDate === undefined) ? firebase.firestore.FieldValue.serverTimestamp() : creationDate;

  const data = {
    location: {
      address: '40 Rue Descartes, 75005 Paris, France',
      country: 'France',
      googlePlaceId: 'ChIJM7NnrOhx5kcRnB5mg6_BHh8',
      locality: 'Paris',
      route: 'Rue Descartes',
      streetNumber: '40'
    },
    steps: [],
    deletionDate: null
  };
  if (creationDate) {
    data.creationDate = creationDate;
  }
  if (ownerId) {
    data.roles = {
      [ownerId]: 'owner'
    };
  }
  if (!skipGeopoint) {
    data.location.geopoint = new firebase.firestore.GeoPoint(48.8456823, 2.3489939);
  }
  return data;
}

exports.getDeepTokenData = function (overwrite) {
  return exports.mergeObjects({
    userId: 'alice',
    type: 'profile',
    creationDate: '2021-01-01T00:00:00Z',
    deletionDate: null
  }, overwrite);
};
