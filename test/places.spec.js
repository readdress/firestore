const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getIdentity, getPlaceData } = require('./setup');

describe('Collection places', () => {
  it('should only let users read their own places', async () => {
    const db = getAuthedFirestore(getIdentity());
    const collection = db.collection('places');
    await firebase.assertFails(collection.get());
    await firebase.assertFails(collection.where('roles.bob', '==', 'owner').get());
    await firebase.assertFails(collection.where('roles.alice', '==', 'reader').get());
    await firebase.assertSucceeds(collection.where('roles.alice', '==', 'owner').get());
  });

  it('can write only on new and owned places', async () => {
    const alice = getAuthedFirestore(getIdentity());
    const bob = getAuthedFirestore(getIdentity('bob'));

    await firebase.assertSucceeds(alice.collection('places').doc('place').set(getPlaceData()));
    await firebase.assertSucceeds(alice.collection('places').doc('place').update({ location: { address: 'new address' } }));
    await firebase.assertFails(bob.collection('places').doc('place').set(getPlaceData()));
    await firebase.assertFails(bob.collection('places').doc('place').update({ location: { address: 'new address' } }));
    await firebase.assertFails(bob.collection('places').doc('place').update({ roles: { bob: 'owner' } }));
    await firebase.assertFails(bob.collection('places').doc('place').update({ 'roles.bob': 'owner' }));
  });

  it('must preserve the owner role', async () => {
    const alice = getAuthedFirestore(getIdentity());
    await firebase.assertSucceeds(alice.collection('places').doc('place').set(getPlaceData()));
    await firebase.assertFails(alice.collection('places').doc('place2').set(getPlaceData('bob')));
    await firebase.assertFails(alice.collection('places').doc('place').update({ roles: { bob: 'owner' } }));
    await firebase.assertFails(alice.collection('places').doc('place').update({ roles: { alice: 'reader' } }));
    await firebase.assertFails(alice.collection('places').doc('place').update({ 'roles.bob': 'owner' })); // Only 1 owner allowed
  });

  it('accept only the allowed fields', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');
    await firebase.assertFails(doc.set({
      ...getPlaceData(),
      otherField: 'other value'
    }));

    await firebase.assertFails(doc.set({
      ...getPlaceData(),
      location: {
        ...(getPlaceData().location),
        otherField: 'other value'
      }
    }));
  });

  it('ensure that the required fields are present', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');

    const { creationDate, ...missingCreationDate } = getPlaceData();
    await firebase.assertFails(doc.set(missingCreationDate));

    const { deletionDate, ...missingDeletionDate } = getPlaceData();
    await firebase.assertFails(doc.set(missingDeletionDate));

    const { address, ...missingAddress } = getPlaceData().location;
    await firebase.assertFails(doc.set({
      ...getPlaceData(),
      location: missingAddress
    }));

    const { steps, ...missingSteps } = getPlaceData();
    await firebase.assertFails(doc.set(missingSteps));

    const { roles, ...missingRoles } = getPlaceData();
    await firebase.assertFails(doc.set(missingRoles));
  });

  it('ensure that fields in location are correct', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');

    const maxFieldsLength = {
      ...getPlaceData(),
      location: {
        address: ''.padEnd(500, '.'),
        country: ''.padEnd(250, '.'),
        geopoint: getPlaceData().location.geopoint,
        googlePlaceId: ''.padEnd(500, '.'),
        locality: ''.padEnd(250, '.'),
        route: ''.padEnd(250, '.'),
        streetNumber: ''.padEnd(250, '.')
      }
    };

    await firebase.assertSucceeds(doc.set(maxFieldsLength));
    await firebase.assertFails(doc.update({ 'location.address': maxFieldsLength.location.address + '.' }));
    await firebase.assertFails(doc.update({ 'location.country': maxFieldsLength.location.country + '.' }));
    await firebase.assertFails(doc.update({ 'location.geopoint': 42 }));
    await firebase.assertFails(doc.update({ 'location.googlePlaceId': maxFieldsLength.location.googlePlaceId + '.' }));
    await firebase.assertFails(doc.update({ 'location.locality': maxFieldsLength.location.locality + '.' }));
    await firebase.assertFails(doc.update({ 'location.route': maxFieldsLength.location.route + '.' }));
    await firebase.assertFails(doc.update({ 'location.streetNumber': maxFieldsLength.location.streetNumber + '.' }));
  });

  it('ensure that there are not too many steps', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');
    const step = {
      type: 'door',
      value: '42'
    };

    await firebase.assertFails(doc.set({
      ...getPlaceData(),
      steps: Array(8).fill(step)
    }));

    await firebase.assertSucceeds(doc.set({
      ...getPlaceData(),
      steps: Array(7).fill(step)
    }));
  });

  [
    { valid: true, type: 'door' },
    { valid: true, type: 'floor' },
    { valid: true, type: 'info' },
    { valid: true, type: 'intercom' },
    { valid: true, type: 'pin' },
    { valid: false, type: 'unknown' },
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the step type ${testCase.type}`, async () => {
      const db = getAuthedFirestore(getIdentity());
      const doc = db.collection('places').doc('place');
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(doc.set({
        ...getPlaceData(),
        steps: [{
          type: testCase.type,
          value: '42'
        }]
      }));
    });
  });

  it('ensure that the step value size is correct', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');
    const step = {
      type: 'door',
      value: '42'
    };

    await firebase.assertFails(doc.set({
      ...getPlaceData(),
      steps: [{
        type: 'door',
        value: ''.padEnd(251, '.')
      }]
    }));

    await firebase.assertSucceeds(doc.set({
      ...getPlaceData(),
      steps: [{
        type: 'door',
        value: ''.padEnd(250, '.')
      }]
    }));
  });

  it('should set the creation date at creation and never change it after', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        creationDate: 'invalid date',
      })
    );

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        creationDate: '2021-01-01T01:00:00Z',
      })
    );

    await firebase.assertSucceeds(
      doc.set(getPlaceData())
    );

    await firebase.assertFails(
      doc.update({
        creationDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );

    await firebase.assertSucceeds(
      doc.update({
        creationDate: (await doc.get()).get('creationDate'),
      })
    );
  });

  /*
  deletion request on place not implemented yet

  it('should ensure that deletionDate is either null, now or unchanged', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        deletionDate: 'invalid',
      })
    );

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        deletionDate: '2021-01-01T01:00:00Z',
      })
    );

    await firebase.assertSucceeds(
      doc.set(getPlaceData())
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: 'invalid'
      })
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: '2021-01-01T01:00:00Z'
      })
    );

    await firebase.assertSucceeds(
      doc.update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
      })
    );

    await firebase.assertSucceeds(
      doc.update({
        deletionDate: null,
      })
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: firebase.firestore.FieldValue.delete()
      })
    );
  });
  */
  it('should ensure that deletionDate is null', async () => {
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('places').doc('place');

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        deletionDate: 'invalid',
      })
    );

    await firebase.assertFails(
      doc.set({
        ...getPlaceData(),
        deletionDate: '2021-01-01T01:00:00Z',
      })
    );

    await firebase.assertSucceeds(
      doc.set(getPlaceData())
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: 'invalid'
      })
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: '2021-01-01T01:00:00Z'
      })
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
      })
    );

    await firebase.assertSucceeds(
      doc.update({
        deletionDate: null,
      })
    );

    await firebase.assertFails(
      doc.update({
        deletionDate: firebase.firestore.FieldValue.delete()
      })
    );
  });
});
