const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getIdentity, getUserData, getCloneData, getCloneId, getFollowerData, adminFollowerData } = require('./setup');

describe('Collection users.followers', () => {

  it('should let the user read only his own followers list', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followers').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').where('deletionDate', '==', null).get()
    );
  });

  it('should let the user read only his own followers details', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set(
      adminFollowerData()
    );
    await admin.collection('users').doc('alice').collection('followers').doc('bob').set(
      adminFollowerData(null, 'bob')
    );

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followers').doc('alice').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').get()
    );
  });

  it('should not prevent access to followers with a deletionDate', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followers').doc('bob').set(
      adminFollowerData(null, 'bob')
    );
    await admin.collection('users').doc('alice').collection('followers').doc('charlie').set(
      adminFollowerData({ deletionDate: '2021-02-01T01:00:00Z' }, 'charlie')
    );

    const db = getAuthedFirestore(getIdentity());
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('charlie').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'bob')
        .get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'charlie')
        .where('deletionDate', '==', null)
        .get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'david')
        .where('deletionDate', '==', null)
        .get()
    );

    // Without the hack, Firestore produces errors about reading the "resource" variable on non existing documents
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers')
        .where(firebase.firestore.FieldPath.documentId(), '==', 'charlie')
        .where('deletionDate', '==', null)
        .get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers')
        .where(firebase.firestore.FieldPath.documentId(), '==', 'david')
        .where('deletionDate', '==', null)
        .get()
    );
  });

  [
    {
      valid: true,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData({ displayName: 'Another name' }),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData({ grantOnly: true }),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: null,
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'allowed',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: null,
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'pending',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: '2021-01-01T00:00:00Z',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: null,
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowedStatus: 'pending',
      afterFollowedStatus: 'allowed',
      followerStatus: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp(),
      setCloneRef: false
    },
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'let' : 'not let'} a user set himself as allowed for the given test case`, async () => {
      const admin = getAdminFirestore();
      await admin.collection('users').doc('alice').set(getUserData());
      const followedDoc = admin.collection('users').doc('alice').collection('followed').doc('bob');
      if (testCase.beforeFollowedStatus) {
        await followedDoc.set({
          status: testCase.beforeFollowedStatus
        });
      }

      const db = getAuthedFirestore(getIdentity());
      const batch = db.batch();
      if (testCase.afterFollowedStatus) {
        batch.update(db.doc(followedDoc.path), { status: testCase.afterFollowedStatus });
      }

      batch.set(db.collection('users').doc('bob').collection('followers').doc('alice'), {
        ...testCase.userData,
        status: testCase.followerStatus,
        updateDate: testCase.updateDate
      });

      if (testCase.setCloneRef) {
        batch.set(db.collection('users').doc('alice')
          .collection('clones').doc(getCloneId('bob', 'followers')), getCloneData('bob'));
      }
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        batch.commit()
      );
    });
  });

  it('should prevent a user from setting himself as a follower', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').set(getUserData());
    const followedDoc = admin.collection('users').doc('alice').collection('followed').doc('alice');
    await followedDoc.set({ status: 'pending' });

    const db = getAuthedFirestore(getIdentity());
    const batch = db.batch();
    batch.update(db.doc(followedDoc.path), { status: 'allowed' });

    batch.set(db.collection('users').doc('alice').collection('followers').doc('alice'), {
      ...getUserData(),
      status: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp()
    });

    batch.set(db.collection('users').doc('alice')
      .collection('clones').doc(getCloneId('alice', 'followers')), getCloneData('alice'));
    await firebase.assertFails(
      batch.commit()
    );
  });

  it('should only allow the user to update his followers', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await admin.collection('users').doc('charlie').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('charlie').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should only allow the user to update the status of his followers', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        otherField: 'not ok',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        displayName: 'Brigitte',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should only allow the user to update the status from pending to allowed or blocked', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'unknown',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'pending',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await admin.collection('users').doc('alice').collection('followers').doc('bob').update({
      status: 'pending',
    });
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'blocked',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await admin.collection('users').doc('alice').collection('followers').doc('bob').update({
      status: 'allowed',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'blocked',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });


  it('should only allow update of status if grantOnly is absent in the update', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        grantOnly: true,
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await admin.collection('users').doc('alice').collection('followers').doc('bob').update({
      grantOnly: true
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        grantOnly: firebase.firestore.FieldValue.delete(),
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should ensure that the updateDate is updated', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: 'not a date'
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: '2021-01-01T00:00:00Z'
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').update({
        status: 'allowed',
        updateDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should let the user delete followers from his own list only', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followers').doc('alice').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').delete()
    );
  });

  it('should let the follower delete himself if status is allowed', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await admin.collection('users').doc('charlie').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followers').doc('alice').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('charlie').collection('followers').doc('alice').delete()
    );
  });

  it('should ensure that there is not followed entry left after deletion', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').delete()
    );

    await admin.collection('users').doc('bob').collection('followed').doc('alice').update({
      status: 'pending',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').delete()
    );

    await admin.collection('users').doc('bob').collection('followed').doc('alice').delete();
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followers').doc('bob').delete()
    );
  });

  it('should ensure that the clone reference is deleted with the object', async () => {
    const admin = getAdminFirestore();

    await admin.collection('users').doc('alice').collection('followers').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await admin.collection('users').doc('bob').collection('clones')
      .doc(getCloneId('alice', 'followers')).set(getCloneData('alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followers').doc('bob').delete()
    );

    const batch = db.batch();
    batch.delete(db.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers')));
    batch.delete(db.collection('users').doc('alice').collection('followers').doc('bob'));

    await firebase.assertSucceeds(
      batch.commit()
    );
  });
});
