const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getCloneId, adminFollowerData, getIdentity, getCloneData } = require('./setup');

describe('Collection users.clones', () => {

  it('should allow clone reference creation only by the user ID', async () => {
    const admin = getAdminFirestore();
    const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers'));
    await admin.collection('users').doc('alice').collection('followers').doc('bob').set(adminFollowerData());

    const alice = getAuthedFirestore(getIdentity());
    const bob = getAuthedFirestore(getIdentity('bob'));
    const charlie = getAuthedFirestore(getIdentity('charlie'));

    await firebase.assertFails(
      charlie.doc(cloneDoc.path).set(getCloneData())
    );
    await firebase.assertSucceeds(
      bob.doc(cloneDoc.path).set(getCloneData())
    );
    await firebase.assertFails(
      alice.doc(cloneDoc.path).set(getCloneData())
    );
  });

  it('should allow only receiverId and relation fields', async () => {
    const admin = getAdminFirestore();
    const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers'));
    await admin.collection('users').doc('alice').collection('followers').doc('bob').set(adminFollowerData());
    await admin.collection('users').doc('alice').collection('followers').doc('bob').collection('places').doc('paris').set({test: true});

    const db = getAuthedFirestore(getIdentity('bob'));

    await firebase.assertFails(
      db.doc(cloneDoc.path).set({})
    );

    const {relation, receiverId} = getCloneData();
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        relation
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        placeId: 'paris'
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation,
        other: 'test'
      })
    );
    await firebase.assertFails(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation,
        placeId: 'paris'
      })
    );
    await firebase.assertSucceeds(
      db.doc(cloneDoc.path).set({
        receiverId,
        relation
      })
    );
  });

  [
    {valid: false},
    {valid: false, relation: ''},
    {valid: false, relation: 'unknown'},
    {valid: false, relation: 'pending'},
    {valid: true, relation: 'followers'},
    {valid: true, relation: 'followed'},
  ].forEach(testCase => {
    it(`should ensure that the relation ${testCase.relation} is ${testCase.valid ? '' : 'in'}valid`, async () => {
      const admin = getAdminFirestore();
      const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', testCase.relation));
      if (testCase.relation && testCase.relation.length) {
        await admin.collection('users').doc('alice').collection(testCase.relation).doc('bob').set(adminFollowerData());
      }

      const db = getAuthedFirestore(getIdentity('bob'));
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;

      const data = testCase.relation !== undefined ? getCloneData(null, testCase.relation): {
        receiverId: getCloneData().receiverId
      };

      await assertFunction(
        db.doc(cloneDoc.path).set(data)
      );
    });
  });

  [
    {valid: false, receiverId: ''},
    {valid: false, receiverId: 'ali.ce'},
    {valid: false, receiverId: 'ali/c/e'},
    {valid: false, receiverId: 'ali[ce'},
    {valid: false, receiverId: 'ali]ce'},
    {valid: false, receiverId: 'ali\\ce'},
    {valid: false, receiverId: 'ali`ce'},
    {valid: false, receiverId: 'alice'.padEnd(129, 'e')},
    {valid: true, receiverId: 'alice'},
    {valid: true, receiverId: 'alice123-_'},
    {valid: true, receiverId: 'alice'.padEnd(128, 'e')},
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the user segment path ${testCase.receiverId.slice(0, 50)}`, async () => {
      const admin = getAdminFirestore();
      const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId(testCase.receiverId, 'followers'));
      if (testCase.receiverId.length > 0) {
        await admin.collection('users').doc(testCase.receiverId).collection('followers').doc('bob').set(adminFollowerData());
      }
      const db = getAuthedFirestore(getIdentity('bob'));
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        db.doc(cloneDoc.path).set(getCloneData(testCase.receiverId))
      );
    });
  });

  [
    {valid: false, id: 'bob_followers'},
    {valid: false, id: 'alice_followers_2'},
    {valid: false, id: '_alice_followers'},
    {valid: false, id: 'alice_pending'},
    {valid: true, id: 'alice_followers'},
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'accept' : 'reject'} the clone ID ${testCase.id}`, async () => {
      const admin = getAdminFirestore();
      const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(testCase.id);
      await admin.collection('users').doc(getCloneData().receiverId).collection('followers').doc('bob').set(adminFollowerData());
      const db = getAuthedFirestore(getIdentity('bob'));
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        db.doc(cloneDoc.path).set(getCloneData())
      );
    });
  });

  it('should allow clone reference creation only if the object is also created', async () => {
    const admin = getAdminFirestore();
      const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers'));
      const db = getAuthedFirestore(getIdentity('bob'));

      await firebase.assertFails(
        db.doc(cloneDoc.path).set(getCloneData())
      );

      await admin.collection('users').doc(getCloneData().receiverId).collection('followers').doc('bob').set(adminFollowerData());

      await firebase.assertSucceeds(
        db.doc(cloneDoc.path).set(getCloneData())
      );
  });

  it('should allow clone deletion only by the user ID or the receiver ID', async () => {
    const admin = getAdminFirestore();
    const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers'));
    await cloneDoc.set(getCloneData());

    const alice = getAuthedFirestore(getIdentity());
    const bob = getAuthedFirestore(getIdentity('bob'));
    const charlie = getAuthedFirestore(getIdentity('charlie'));

    await firebase.assertFails(
      charlie.doc(cloneDoc.path).delete()
    );
    await firebase.assertSucceeds(
      alice.doc(cloneDoc.path).delete()
    );
    await firebase.assertFails(
      bob.doc(cloneDoc.path).delete()
    );
    await cloneDoc.set(getCloneData());
    await firebase.assertSucceeds(
      bob.doc(cloneDoc.path).delete()
    );
  });

  it('should allow clone reference deletion only if the object is also deleted', async () => {
    const admin = getAdminFirestore();
    const followerDoc = admin.collection('users').doc('alice').collection('followers').doc('bob');
    const cloneDoc = admin.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followers'));
    await followerDoc.set(adminFollowerData());

    await cloneDoc.set(getCloneData());

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.doc(cloneDoc.path).delete()
    );

    const batch = db.batch();
    batch.delete(db.doc(followerDoc.path));
    batch.delete(db.doc(cloneDoc.path));
    await firebase.assertSucceeds(
      batch.commit()
    );
  });
});
