const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getIdentity, getCloneData, getCloneId, getPlaceData } = require('./setup');

describe('Collection users.followed.places', () => {

  it('should let only the user and the followed read the places list', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('charlie').collection('places').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('bob').collection('followed').doc('alice').collection('places').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').where('deletionDate', '==', null).get()
    );
  });

  it('should let only the user and the followed read the places details', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('bob').collection('followed').doc('charlie').collection('places').doc('paris').set(
      getPlaceData('charlie', '2021-01-01T01:00:00Z', true)
    );
    await admin.collection('users').doc('bob').collection('followed').doc('alice').collection('places').doc('paris').set(
      getPlaceData('alice', '2021-01-01T01:00:00Z', true)
    );
    await admin.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').set(
      getPlaceData('bob', '2021-01-01T01:00:00Z', true)
    );

    const db = getAuthedFirestore(getIdentity());
    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('charlie').collection('places').doc('paris').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('bob').collection('followed').doc('alice').collection('places').doc('paris').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').get()
    );
  });

  it('should prevent access to places with a deletionDate', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').set(
      getPlaceData('bob', '2021-01-01T01:00:00Z', true)
    );
    await admin.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('marseille').set({
      ...getPlaceData('bob', '2021-01-01T01:00:00Z', true),
      deletionDate: '2021-02-01T01:00:00Z'
    });

    const db = getAuthedFirestore(getIdentity());
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').get()
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('marseille').get()
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('lyon').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'paris')
        .where('deletionDate', '==', null)
        .get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'marseille')
        .where('deletionDate', '==', null)
        .get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places')
        .where(firebase.firestore.FieldPath.documentId(), '!=', 'fake_id') // Hack to force Firestore query the collection
        .where(firebase.firestore.FieldPath.documentId(), '==', 'lyon')
        .where('deletionDate', '==', null)
        .get()
    );

    // // Without the hack, Firestore produces errors about reading the "resource" variable on non existing documents
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places')
        .where(firebase.firestore.FieldPath.documentId(), '==', 'marseille')
        .where('deletionDate', '==', null)
        .get()
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places')
        .where(firebase.firestore.FieldPath.documentId(), '==', 'lyon')
        .where('deletionDate', '==', null)
        .get()
    );
  });

  it('should let the followed user add followed places', async () => {
    const admin = getAdminFirestore();
    await admin.collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));
    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'allowed' });
    await admin.collection('users').doc('bob').collection('followed').doc('charlie').set({ status: 'allowed' });
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'charlie')).set(getCloneData('bob', 'charlie'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('charlie')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
  });

  it('should let the user add self-written places', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    await admin.collection('users').doc('alice').collection('followed').doc('charlie').set({ status: 'allowed' });
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false))
    );
    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('charlie')
        .collection('places').doc('paris').set(getPlaceData(false))
    );
    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('david')
        .collection('places').doc('paris').set(getPlaceData(false))
    );
  });


  it('should ensure that the followed user is owning the place', async () => {
    const admin = getAdminFirestore();
    await admin.collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));
    await admin.collection('places').doc('marseille').set(getPlaceData('charlie', '2021-01-01T01:00:00Z', true));
    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'allowed' });
    await admin.collection('users').doc('bob').collection('followed').doc('charlie').set({ status: 'allowed' });
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));
    await admin.collection('places').doc('marseille').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('marseille').set(getPlaceData('charlie', '2021-01-01T01:00:00Z', true))
    );
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('marseille').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
  });

  it('should ensure that only the allowed fields are set', async () => {
    const admin = getAdminFirestore();
    await admin.collection('places').doc('paris').set({
      ...getPlaceData('alice', '2021-01-01T01:00:00Z', true),
      other: 'other'
    });
    await admin.collection('places').doc('marseille').set({
      ...getPlaceData('alice', '2021-01-01T01:00:00Z', true),
      location: {
        ...getPlaceData('alice', '2021-01-01T01:00:00Z', true).location,
        other: 'other'
      }
    });
    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'allowed' });
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));
    await admin.collection('places').doc('marseille').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set({
          ...getPlaceData('alice', '2021-01-01T01:00:00Z', true),
          other: 'other'
        })
    );
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('marseille').set({
          ...getPlaceData('alice', '2021-01-01T01:00:00Z', true),
          location: {
            ...getPlaceData('alice', '2021-01-01T01:00:00Z', true).location,
            other: 'other'
          }
        })
    );
  });

  it('should ensure that only the allowed fields are set for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false),
          other: 'other',
        })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false),
          location: {
            ...getPlaceData(false).location,
            other: 'other'
          }
        })
    );

    await firebase.assertSucceeds(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false))
    );
  });

  it('should ensure that the followed place is identical to the source place', async () => {
    const placeData = {
      ...getPlaceData('alice', '2021-01-01T01:00:00Z', true),
      steps: [
        { type: 'floor', value: 8 },
        { type: 'door', value: 42 },
      ]
    };

    const admin = getAdminFirestore();
    await admin.collection('places').doc('paris').set(placeData);
    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'allowed' });
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());
    const followedPlace = db.collection('users').doc('bob').collection('followed').doc('alice').collection('places').doc('paris');

    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        creationDate: '2021-01-01T02:00:00Z'
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        deletionDate: '2021-01-01T02:00:00Z'
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        location: {
          ...placeData.location,
          country: 'US'
        }
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        location: {
          ...placeData.location,
          other: 'other'
        }
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        roles: {
          ...placeData.roles,
          other: 'owner'
        }
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        steps: []
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        steps: [
          { type: 'floor', value: 8 }
        ]
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        steps: [
          { type: 'door', value: 42 },
          { type: 'floor', value: 8 },
        ]
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        steps: [
          { type: 'floor', value: 8 },
          { type: 'door', value: 42 },
          { type: 'intercom', value: 50 },
        ]
      })
    );
    await firebase.assertFails(
      followedPlace.set({
        ...placeData,
        steps: [
          { type: 'floor', value: 8 },
          { type: 'door', value: 42, other: 1 },
        ]
      })
    );
    await firebase.assertSucceeds(
      followedPlace.set({
        ...placeData,
        steps: [
          { type: 'floor', value: 8 },
          { type: 'door', value: 42 },
        ]
      })
    );
  });

  it('should ensure that the creation date is now for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, false))
    );

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, '2021-01-01T01:00:00Z'))
    );

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, 'invalid date'))
    );

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, 'invalid date'))
    );

    await firebase.assertSucceeds(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, firebase.firestore.FieldValue.serverTimestamp()))
    );
  });

  it('should ensure that the deletionDate is null for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false, firebase.firestore.FieldValue.serverTimestamp()),
          deletionDate: '2021-01-01T01:00:00Z'
        })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false, firebase.firestore.FieldValue.serverTimestamp()),
          deletionDate: 'something'
        })
    );

    const { deletionDate, ...missingDeletionDate } = getPlaceData(false, firebase.firestore.FieldValue.serverTimestamp())
    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(missingDeletionDate)
    );

    await firebase.assertSucceeds(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set(getPlaceData(false, firebase.firestore.FieldValue.serverTimestamp()))
    );
  });

  it('should ensure that fields in location are correct for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');

    await firebase.assertFails(doc.set({
      ...getPlaceData(false),
      location: {
        address: ''.padEnd(501, '.'),
      }
    }));
    await firebase.assertSucceeds(doc.set({
      ...getPlaceData(false),
      location: {
        address: ''.padEnd(500, '.'),
      }
    }));
  });

  it('should validate the steps for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());
    const doc = db.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');

    await firebase.assertFails(doc.set({
      ...getPlaceData(false),
      steps: [{
        type: 'invalid',
        value: '42'
      }]
    }));
    await firebase.assertSucceeds(doc.set({
      ...getPlaceData(false),
      steps: [{
        type: 'door',
        value: '42'
      }]
    }));
  });

  it('ensure that the self-written place does not have too many steps', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const db = getAuthedFirestore(getIdentity());

    const step = {
      type: 'door',
      value: '42'
    };

    await firebase.assertFails(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false),
          steps: Array(8).fill(step)
        }));

    await firebase.assertSucceeds(
      db.collection('users').doc('alice')
        .collection('followed').doc('bob')
        .collection('places').doc('paris').set({
          ...getPlaceData(false),
          steps: Array(7).fill(step)
        }));
  });

  it('should ensure that the followed user is an allowed followed', async () => {
    const admin = getAdminFirestore();
    await admin.collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true));
    await admin.collection('places').doc('paris').collection('clones').doc(getCloneId('bob', 'alice')).set(getCloneData('bob', 'alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );

    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'pending' });
    await firebase.assertFails(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );

    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({ status: 'allowed' });
    await firebase.assertSucceeds(
      db.collection('users').doc('bob')
        .collection('followed').doc('alice')
        .collection('places').doc('paris').set(getPlaceData('alice', '2021-01-01T01:00:00Z', true))
    );
  });


  it('should let the user update the place details if self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const docBob = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await docBob.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    await admin.collection('users').doc('alice').collection('followed').doc('charlie').set({ status: 'allowed' });
    const docCharlie = admin.collection('users').doc('alice')
      .collection('followed').doc('charlie')
      .collection('places').doc('madrid');
    await docCharlie.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertSucceeds(db.doc(docBob.path).update({
      'location.address': 'somewhere'
    }));
    await firebase.assertFails(db.doc(docCharlie.path).update({
      'location.address': 'somewhere'
    }));
  });

  it('should ensure that the creationDate is not changed at self-written place update', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(db.doc(doc.path).update({
      'location.address': 'somewhere',
      'creationDate': firebase.firestore.FieldValue.serverTimestamp()
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      'location.address': 'somewhere'
    }));
  });


  it('should ensure that the deletionDate is not changed at self-written place update', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(db.doc(doc.path).update({
      'location.address': 'somewhere',
      'deletionDate': firebase.firestore.FieldValue.serverTimestamp()
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      'location.address': 'somewhere'
    }));
  });

  it('should ensure that only the allowed fields are in self-written place update', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(db.doc(doc.path).update({
      other: 'field'
    }));
    await firebase.assertFails(db.doc(doc.path).update({
      'location.other': 'field'
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      'location.address': 'somewhere'
    }));
  });


  it('should ensure that fields in location are correct for self-written update', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(db.doc(doc.path).update({
      'location.address': ''.padEnd(501, '.')
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      'location.address': ''.padEnd(500, '.')
    }));
  });

  it('should validate the steps for self-written', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(db.doc(doc.path).update({
      steps: [{
        type: 'invalid',
        value: '42'
      }]
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      steps: [{
        type: 'door',
        value: '42'
      }]
    }));
  });

  it('ensure that the self-written place does not have too many steps', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({ status: 'self-written' });
    const doc = admin.collection('users').doc('alice')
      .collection('followed').doc('bob')
      .collection('places').doc('paris');
    await doc.set({
      ...getPlaceData(false, '2021-01-01T01:00:00Z', true),
    });

    const db = getAuthedFirestore(getIdentity());

    const step = {
      type: 'door',
      value: '42'
    };

    await firebase.assertFails(db.doc(doc.path).update({
      steps: Array(8).fill(step)
    }));
    await firebase.assertSucceeds(db.doc(doc.path).update({
      steps: Array(7).fill(step)
    }));
  });

  it('should let the user and the followed delete places from the followed list', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('charlie').collection('places').doc('paris').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('bob').collection('followed').doc('alice').collection('places').doc('paris').delete()
    );
  });

  it('should ensure that the clone reference is deleted with the object', async () => {
    const admin = getAdminFirestore();

    await admin.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris')
      .set(getPlaceData('bob', '2021-01-01T00:00:00Z', true));

    await admin.collection('places').doc('paris').collection('clones')
      .doc(getCloneId('alice', 'bob')).set(getCloneData('alice', 'bob'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris').delete()
    );

    const batch = db.batch();
    batch.delete(db.collection('places').doc('paris').collection('clones').doc(getCloneId('alice', 'bob')));
    batch.delete(db.collection('users').doc('alice').collection('followed').doc('bob').collection('places').doc('paris'));

    await firebase.assertSucceeds(
      batch.commit()
    );
  });
});
