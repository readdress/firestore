var assert = require('assert');
const { mergeObjects } = require('./setup');

describe('Setup helpers', function() {
  it('should merge objects', function() {
    assert.deepStrictEqual(mergeObjects({
      prop1: 'value 1',
    }, {
      prop2: 'value 2',
    }), {
      prop1: 'value 1',
      prop2: 'value 2',
    });

    assert.deepStrictEqual(mergeObjects({
      prop1: 'value 1',
    }, {
      prop1: 'value 2',
    }), {
      prop1: 'value 2',
    });

    assert.deepStrictEqual(mergeObjects({
      prop1: {sub1: 'value 1'},
    }, {
      prop1: {sub2: 'value 2'},
    }), {
      prop1: {sub2: 'value 2'},
    });
  });

  it('should leave obj1 unchanged if obj2 is omitted', function() {
    assert.deepStrictEqual(mergeObjects({
      prop1: 'value 1',
    }), {
      prop1: 'value 1',
    });
  });

  it('should remove obj1 props when explicitly set to undefined in obj2', function() {
    assert.deepStrictEqual(mergeObjects({
      prop1: 'value 1',
      prop2: 'value 2',
    }, {
      prop2: undefined,
    }), {
      prop1: 'value 1',
    });
  });
});
