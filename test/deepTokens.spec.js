const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getIdentity, getDeepTokenData } = require('./setup');

describe('Collection deepTokens', () => {

  it('should let the user read only his own tokens', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('deepTokens').where('userId', '==', 'bob').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('deepTokens').where('userId', '==', 'alice').where('deletionDate', '==', null).get()
    );
  });

  it('should let the user read only the tokens that are not revoked', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('deepTokens').where('userId', '==', 'alice').get()
    );
    await firebase.assertFails(
      db.collection('deepTokens').where('userId', '==', 'alice').where('deletionDate', '==', '2021-01-01T00:00:00Z').get()
    );
    await firebase.assertFails(
      db.collection('deepTokens').where('userId', '==', 'alice').where('deletionDate', '>', '2099-01-01T00:00:00Z').get()
    );
    await firebase.assertSucceeds(
      db.collection('deepTokens').where('userId', '==', 'alice').where('deletionDate', '==', null).get()
    );
  });

  it('should forbid token creation', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('deepTokens').add(getDeepTokenData())
    );
  });

  it('should allow the user to update only his token', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('deepTokens').doc('token_alice').set(getDeepTokenData());
    await admin.collection('deepTokens').doc('token_bob').set(getDeepTokenData({ userId: 'bob' }));

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_bob').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should allow to update only the deletionDate field', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('deepTokens').doc('token_alice').set(getDeepTokenData());

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        unknownField: 'value'
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        userId: 'bob'
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        type: 'sharing'
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        grant: ['paris']
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        grantOnly: true
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        creationDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
    await firebase.assertSucceeds(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );

    await admin.collection('deepTokens').doc('token_alice2').set(getDeepTokenData({
      type: 'sharing',
      grant: ['paris'],
      grantOnly: true
    }));
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice2').update({
        grant: ['paris', 'marseille']
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice2').update({
        grantOnly: false
      })
    );
    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice2').update({
        grantOnly: firebase.firestore.FieldValue.delete()
      })
    );
    await firebase.assertSucceeds(
      db.collection('deepTokens').doc('token_alice2').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should allow to update only if the deletionDate field was null', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('deepTokens').doc('token_alice').set(getDeepTokenData());
    await admin.collection('deepTokens').doc('token_alice_revoked').set(getDeepTokenData({ deletionDate: '2021-01-01T00:00:00Z' }));

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice_revoked').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );

    await firebase.assertSucceeds(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should allow to update the deletionDate to the current timestamp only', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('deepTokens').doc('token_alice').set(getDeepTokenData());

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: 'hello'
      })
    );

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: '2021-01-01T00:00:00Z'
      })
    );

    await firebase.assertSucceeds(
      db.collection('deepTokens').doc('token_alice').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp()
      })
    );
  });

  it('should forbid token deletion', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('deepTokens').doc('token_alice').set(getDeepTokenData());

    await firebase.assertFails(
      db.collection('deepTokens').doc('token_alice').delete()
    );
  });
});
