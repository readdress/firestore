const firebase = require('@firebase/rules-unit-testing');
const { getAuthedFirestore, getAdminFirestore, getIdentity, getUserData, getCloneData, getCloneId, getFollowedData } = require('./setup');

describe('Collection users.followed', () => {

  it('should let the user read only his own followed list', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').where('deletionDate', '==', null).get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').where('deletionDate', '==', null).get()
    );
  });

  it('should let the user read only his own followed details', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('bob').collection('followed').doc('alice').set(
      getFollowedData(null, 'alice')
    );
    await admin.collection('users').doc('alice').collection('followed').doc('bob').set(
      getFollowedData(null, 'bob')
    );

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('alice').get()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').get()
    );
  });

  it('should disallow the creation of followed docs', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').set({})
    );
  });

  [
    {
      valid: true,
      userData: getUserData(),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData({ displayName: 'Another name' }),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: null,
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'blocked',
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'allowed',
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: null,
      followedStatus: 'allowed',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: 'allowed',
      followedStatus: 'pending',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: 'allowed',
      followedStatus: 'blocked',
      setCloneRef: true
    },
    {
      valid: false,
      userData: getUserData(),
      beforeFollowerStatus: 'pending',
      afterFollowerStatus: 'allowed',
      followedStatus: 'allowed',
      setCloneRef: false
    },
  ].forEach(testCase => {
    it(`should ${testCase.valid ? 'let' : 'not let'} a user set himself as allowed for the given test case`, async () => {
      const admin = getAdminFirestore();
      await admin.collection('users').doc('alice').set(getUserData());
      const followerDoc = admin.collection('users').doc('alice').collection('followers').doc('bob');
      if (testCase.beforeFollowerStatus) {
        await followerDoc.set({
          status: testCase.beforeFollowerStatus,
          deletionDate: null
        });
      }

      const db = getAuthedFirestore(getIdentity());
      const batch = db.batch();
      if (testCase.afterFollowerStatus) {
        batch.update(db.doc(followerDoc.path), {
          status: testCase.afterFollowerStatus,
          updateDate: firebase.firestore.FieldValue.serverTimestamp()
        });
      }

      batch.set(db.collection('users').doc('bob').collection('followed').doc('alice'), {
        ...testCase.userData,
        status: testCase.followedStatus,
      });

      if (testCase.setCloneRef) {
        batch.set(db.collection('users').doc('alice')
          .collection('clones').doc(getCloneId('bob', 'followed')), getCloneData('bob', 'followed'));
      }
      const assertFunction = testCase.valid ? firebase.assertSucceeds : firebase.assertFails;
      await assertFunction(
        batch.commit()
      );
    });
  });

  it('should prevent a user from setting himself as a followed', async () => {
    const admin = getAdminFirestore();
    await admin.collection('users').doc('alice').set(getUserData());
    const followerDoc = admin.collection('users').doc('alice').collection('followers').doc('alice');
    await followerDoc.set({ status: 'pending' });

    const db = getAuthedFirestore(getIdentity());
    const batch = db.batch();
    batch.update(db.doc(followerDoc.path), {
      status: 'allowed',
      updateDate: firebase.firestore.FieldValue.serverTimestamp()
    });

    batch.set(db.collection('users').doc('alice').collection('followed').doc('alice'), {
      ...getUserData(),
      status: 'allowed',
    });

    batch.set(db.collection('users').doc('alice')
      .collection('clones').doc(getCloneId('alice', 'followed')), getCloneData('alice', 'followed'));

    await firebase.assertFails(
      batch.commit()
    );
  });

  it('should let the user create self-written profiles', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'unknown',
        deletionDate: null
      })
    );

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'self-written',
        deletionDate: null
      })
    );
  });

  it('should validate the fields of self-written profiles at creation', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'self-written',
        deletionDate: null,
        other: 'field'
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        status: 'self-written',
        deletionDate: null,
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: '',
        status: 'self-written',
        deletionDate: null,
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: ''.padEnd(251, 'Bob'),
        status: 'self-written',
        deletionDate: null,
      })
    );

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: ''.padEnd(250, 'Bob'),
        status: 'self-written',
        deletionDate: null,
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'self-written',
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'self-written',
        deletionDate: '2021-01-01T00:00:00Z',
      })
    );

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').add({
        displayName: 'Bob',
        status: 'self-written',
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
      })
    );
  });

  it('should only allow the user to update his followed', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await admin.collection('users').doc('charlie').collection('followed').doc('bob').set({
      ...getUserData(null, 'charlie'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('followers').doc('charlie').set({
      ...getUserData(null, 'charlie'),
      status: 'allowed',
    });

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('charlie').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
  });

  it('should only allow to update the status field', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        other: 'field',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        email: 'charlie@example.com',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: 'Charlie',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        photoURL: 'https://example.com',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
  });

  it('should only allow to update the displayName field for self-written profiles', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      displayName: 'Bob',
      status: 'self-written',
    });

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        other: 'field',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        email: 'charlie@example.com',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        photoURL: 'https://example.com',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        deletionDate: firebase.firestore.FieldValue.serverTimestamp(),
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: 'Bob updated',
      })
    );
  });

  it('should ensure that the updated displayName field for self-written profiles is valid', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      displayName: 'Bob',
      status: 'self-written',
    });

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: null,
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: '',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: ''.padEnd(251, 'Bob'),
      })
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        displayName: ''.padEnd(250, 'Bob'),
      })
    );
  });

  it('should only allow updates if the status was pending', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await admin.collection('users').doc('alice').collection('followed').doc('charlie').set({
      ...getUserData(null, 'charlie'),
      status: 'allowed',
    });
    await admin.collection('users').doc('charlie').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });

    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('charlie').update({
        status: 'allowed',
      })
    );
  });

  it('should only allow updates if the status will be allowed', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'pending',
      })
    );
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'blocked',
      })
    );
  });

  it('should only allow updates if the user will be set as follower', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').update({
        status: 'allowed',
      })
    );
  });

  it('should let the user delete followed from his own list only', async () => {
    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('alice').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );
  });

  it('should let the followed delete himself if status is allowed', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('bob').collection('followed').doc('alice').set({
      ...getUserData(),
      status: 'pending',
    });
    await admin.collection('users').doc('charlie').collection('followed').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
    });
    await firebase.assertFails(
      db.collection('users').doc('bob').collection('followed').doc('alice').delete()
    );
    await firebase.assertSucceeds(
      db.collection('users').doc('charlie').collection('followed').doc('alice').delete()
    );
  });

  it('should ensure that there is not allowed follower entry left after deletion', async () => {
    const admin = getAdminFirestore();
    const db = getAuthedFirestore(getIdentity());

    await admin.collection('users').doc('bob').collection('followers').doc('alice').set({
      ...getUserData(),
      status: 'allowed',
      updateDate: '2021-01-01T00:00:00Z'
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );

    await admin.collection('users').doc('bob').collection('followers').doc('alice').update({
      status: 'pending',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );

    await admin.collection('users').doc('bob').collection('followers').doc('alice').update({
      status: 'blocked',
    });
    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );

    await admin.collection('users').doc('bob').collection('followers').doc('alice').delete();
    await firebase.assertSucceeds(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );
  });

  it('should ensure that the clone reference is deleted with the object', async () => {
    const admin = getAdminFirestore();

    await admin.collection('users').doc('alice').collection('followed').doc('bob').set({
      ...getUserData(null, 'bob'),
      status: 'pending',
    });
    await admin.collection('users').doc('bob').collection('clones')
      .doc(getCloneId('alice', 'followed')).set(getCloneData('alice'));

    const db = getAuthedFirestore(getIdentity());

    await firebase.assertFails(
      db.collection('users').doc('alice').collection('followed').doc('bob').delete()
    );

    const batch = db.batch();
    batch.delete(db.collection('users').doc('bob').collection('clones').doc(getCloneId('alice', 'followed')));
    batch.delete(db.collection('users').doc('alice').collection('followed').doc('bob'));

    await firebase.assertSucceeds(
      batch.commit()
    );
  });
});
