# ReAddress Firestore

ReAddress is relying on [Cloud Firestore](https://firebase.google.com/docs/firestore) to store and retrieve data.
This repository contains:

* The [Cloud Firestore security rules](firestore.rules) in place for access control and data validation,
* The [Cloud Storage security rules](storage.rules),
* The [automated tests](test) to verify that the security rules behave as expected.

**If you think you may have found a security vulnerability, please [consult our security reports page](https://readdress.app/en/legal/security/).**

## Testing

* Install the emulator: `firebase setup:emulators:firestore`
* Install the test dependencies: `npm install`
* Run the tests: `firebase emulators:exec --only firestore "npm run test"`
* Run a precise test: `firebase emulators:exec --only firestore "npm run test -- -g places"`

## Update indexes from console

To align `firestore.indexes.json` with some changes done on Firebase console,
run this from the `firestore` folder:

```bash
firebase firestore:indexes > firestore.indexes.json
```

## Deploy

* Deploy Firestore rules and indexes: `firebase deploy --only firestore`
* Deploy Storage rules: `firebase deploy --only storage`

## Sources

* https://firebase.google.com/docs/firestore/security/test-rules-emulator
* https://github.com/firebase/quickstart-testing/tree/master/unit-test-security-rules
